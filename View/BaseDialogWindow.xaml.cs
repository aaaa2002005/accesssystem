﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AccessControlSystem
{
    /// <summary>
    /// Interaction logic for BaseDialogWindow
    /// </summary>
    public partial class BaseDialogWindow : Window
    {
        /// <summary>
        /// The view model of BaseDialogWindow
        /// </summary>
        private BaseDialogViewModel viewModel;

        /// <summary>
        /// The view model of BaseDialogWindow
        /// </summary>
        public BaseDialogViewModel ViewModel
        {
            get => viewModel;
            set
            {
                viewModel = value;
                this.DataContext = viewModel;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BaseDialogWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the window
        /// </summary>
        public new void Close()
        {
            this.DialogResult = false;
            base.Close();
        }

        /// <summary>
        /// Finish this window's business logic and close it
        /// </summary>
        public void Finish()
        {
            this.DialogResult = true;
            base.Close();
        }
    }
}
