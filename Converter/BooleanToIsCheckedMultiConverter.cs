﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace AccessControlSystem
{
    /// <summary>
    /// Convert boolean value to IsChecked, it means that using the IsChosen property to bind the IsChecked(IsSelected).
    /// </summary>
    /// <params> 
    /// values = bool[2] {IsSelected(IsChecked), IsChosen}
    /// </params>
    class BooleanToIsCheckedMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)values[1] == true ? true : false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[2] { true, false };
        }
    }
}
