﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace AccessControlSystem
{
    /// <summary>
    /// Convert boolean value to BorderBrush. If true use the WorldBlue, else ThemeDrakGray
    /// </summary>
    public class BooleanToBorderBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Application.Current.TryFindResource("BlackBrush") : Application.Current.TryFindResource("DarkGrayBrush");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
