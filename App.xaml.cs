﻿using AccessControlSystem.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace AccessControlSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Custom startup so can load some time-consuming module
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            //Init the splash screen
            SplashScreen screen = new SplashScreen("/Asset/Image/PowerGridLogo.jpg");

            //Show and set auto close
            screen.Show(true);

            //Load the modules
            Load();

            //Start the base application 
            base.OnStartup(e);

            //Start the MainWindow
            Current.MainWindow = new MainWindow();
            Current.MainWindow.Show();
        }

        /// <summary>
        /// Load time-consuming module
        /// </summary>
        private void Load()
        {
            //Init face recognition models
            FaceRecognizer.InitModel();

        }

    }
}
