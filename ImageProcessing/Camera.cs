﻿using OpenCvSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessControlSystem.ImageProcessing
{
    /// <summary>
    /// Camera class
    /// </summary>
    public class Camera
    {
        #region Private Members

        /// <summary>
        /// A VideoCapture object
        /// </summary>
        private VideoCapture videoCapture;

        /// <summary>
        /// Whether camera is opened
        /// </summary>
        private bool isOpened = false;

        #endregion

        #region Public Properties

        /// <summary>
        /// The height of frame
        /// </summary>
        public int FrameHeight
        {
            get => videoCapture.FrameHeight;
            set => videoCapture.FrameHeight = value;
        }

        /// <summary>
        /// The width of frame
        /// </summary>
        public int FrameWidth
        {
            get => videoCapture.FrameWidth;
            set => videoCapture.FrameWidth = value;
        }

        /// <summary>
        /// Whether camera is opened
        /// </summary>
        public bool IsOpened
        {
            get => isOpened;
            set => isOpened = value;
        }

        #endregion


        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="index"> 
        /// camera index
        /// Note:
        /// See the VideoCapture(OpenCvSharp)
        /// </param>
        public Camera(int index)
        {
            Open(index);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Open camera using index
        /// </summary>
        /// <param name="index"> 
        /// camera index
        /// Note:
        /// See the VideoCapture(OpenCvSharp)
        /// </param>
        public void Open(int index)
        {
            if (videoCapture != null)
            {
                Close();
            }
            videoCapture = new VideoCapture(index);
            isOpened = true;
        }

        /// <summary>
        /// Close camera
        /// </summary>
        public void Close()
        {
            IsOpened = false;
        }

        /// <summary>
        /// Release camera
        /// </summary>
        public void Release()
        {
            videoCapture.Release();
            videoCapture.Dispose();
        }

        /// <summary>
        /// Read a frame from camera
        /// </summary>
        /// <returns>A frame which's format is Mat(OpenCvSharp)</returns>
        public Mat Read()
        {
            Mat frame = new Mat();
            return videoCapture.Read(frame) ? frame : null;
        }

        /// <summary>
        /// Get all camera could be opened
        /// </summary>
        /// <returns>
        /// </returns>
        public static IEnumerable<int> GetAllCameraIndexes()
        {
            List<int> resultList = new List<int>();
            int index = 0;
            var camera = VideoCapture.FromCamera(index);

            //Foreach the all cameras could be opened
            while (camera.IsOpened())
            {
                resultList.Add(index++);
                camera.Dispose();
                camera = VideoCapture.FromCamera(index);
            }

            return resultList;
        }

        #endregion
    }
}
