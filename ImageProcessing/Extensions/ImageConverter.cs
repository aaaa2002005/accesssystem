﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using FaceRecognitionDotNet;
using System.Runtime.InteropServices;
using OpenCvSharp.Extensions;

namespace AccessControlSystem.ImageProcessing.Extensions
{
    /// <summary>
    /// The converter to provide the static public methods to convert image format
    /// </summary>
    public static class ImageConverter
    {
        #region Public Methods

        /// <summary>
        /// Convert BitmapImage to Mat(OpenCVSharp)
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Mat ToMat(BitmapImage image)
        {
            return BitmapSourceConverter.ToMat(image).Clone();
        }

        /// <summary>
        /// Convert the Mat(OpenCvSharp) to BitmapImage
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static BitmapImage ToBitmapImage(Mat image)
        {
            BitmapSource source = BitmapSourceConverter.ToBitmapSource(image);
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            BitmapImage bitmapImage = new BitmapImage();
            MemoryStream memoryStream = new MemoryStream();

            encoder.Frames.Add(BitmapFrame.Create(source));
            encoder.Save(memoryStream);
            memoryStream.Position = 0;

            //Write the source and freeze it to make thread-safe
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
            bitmapImage.EndInit();
            bitmapImage.Freeze();

            return bitmapImage;
        }

        /// <summary>
        /// Convert the Mat(OpenCvSharp) to Image(FaceRecognitionDotNet)
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Image ToImage(Mat image)
        {
            //Convert argb to rgb
            if (image.Channels() == 4)
            {
                Cv2.CvtColor(image, image, ColorConversionCodes.BGRA2BGR);
            }
            byte[] imageData = new byte[image.Rows * image.Cols * image.ElemSize()];
            Marshal.Copy(image.Data, imageData, 0, imageData.Length);
            return FaceRecognition.LoadImage(imageData, image.Rows, image.Cols, image.ElemSize());
        }

        /// <summary>
        /// Convert the BitmapImage to Image(FaceRecognitionDotNet)
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Image ToImage(BitmapImage image)
        {
            return ToImage(ToMat(image));
        }


        #endregion
    }
}
