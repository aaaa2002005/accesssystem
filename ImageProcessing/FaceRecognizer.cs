﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using AccessControlSystem.ImageProcessing.Extensions;
using FaceRecognitionDotNet;

namespace AccessControlSystem.ImageProcessing
{
    /// <summary>
    /// The static face recognizer
    /// </summary>
    public static class FaceRecognizer
    {
        #region Private Members

        /// <summary>
        /// A face recognition model 
        /// </summary>
        private static FaceRecognition faceRecognition;

        #endregion

        #region Public Methods

        /// <summary>
        /// Init the model of FaceRecognitionDotNet
        /// </summary>
        public static void InitModel()
        {
            //The default path is /bin/x64/Debug(Release)
            faceRecognition = FaceRecognition.Create("../../../ImageProcessing/Models");
        }

        /// <summary>
        /// Validate the photo whether can face encoding
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static bool IsCanFaceEncoding(Image image)
        {
            return faceRecognition.FaceEncodings(image).ToArray().Length == 1 ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="outputImage"></param>
        /// <returns></returns>
        public static bool DetectFace(Mat frame, out BitmapImage outputImage)
        {
            outputImage = null;
            Image image = ImageConverter.ToImage(frame);
            Location[] faceLocations = faceRecognition.FaceLocations(image, 1, Model.Hog).ToArray();
            if(faceLocations.Length == 0)
            {
                return false;
            }

            Location maxFace = (from l in faceLocations
                                orderby ((l.Bottom - l.Top) * (l.Right - l.Left)) descending
                                select l).First();

            //Draw rectangle in image
            Rect faceRect = new Rect(maxFace.Left, maxFace.Top, maxFace.Right - maxFace.Left, maxFace.Bottom - maxFace.Top);
            Cv2.Rectangle(frame, faceRect, new Scalar(0, 0, 255), 4);
            outputImage = ImageConverter.ToBitmapImage(frame);

            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="knownFace"></param>
        /// <param name="detectedFace"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public static bool Recognize(Image knownFace, Image detectedFace, int threshold = 60)
        {
            var knownFaceEncoding = faceRecognition.FaceEncodings(knownFace, null, 1, PredictorModel.Large).ToArray()[0];
            var detectedFaceEncoding = faceRecognition.FaceEncodings(detectedFace, null, 1, PredictorModel.Large).ToArray()[0];
            return FaceRecognition.CompareFace(knownFaceEncoding, detectedFaceEncoding, (double)(100 - threshold) / 100);
        }

        #endregion
    }
}
