﻿using AccessControlSystem.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AccessControlSystem
{
    /// <summary>
    /// The view model of HomePage to implement it's business logic
    /// </summary>
    public class HomeViewModel : ViewModelBase
    {

        #region Private Members

        /// <summary>
        /// The service of worker
        /// </summary>
        private WorkerService workerService = new WorkerService();

        /// <summary>
        /// The pie chart's label action
        /// </summary>
        private Func<ChartPoint, string> pointLabel;

        /// <summary>
        /// The spearator of list's content
        /// </summary>
        private string spearator = "   ";

        /// <summary>
        /// The list of attendance
        /// </summary>
        private string attendanceList;

        /// <summary>
        /// The list of absence
        /// </summary>
        private string absenceList;

        /// <summary>
        /// The list of late
        /// </summary>
        private string lateList;

        /// <summary>
        /// The count of attendance 
        /// </summary>
        private ChartValues<int> attendanceCount;

        /// <summary>
        /// The count of absence
        /// </summary>
        private ChartValues<int> absenceCount;

        /// <summary>
        /// The count of late
        /// </summary>
        private ChartValues<int> lateCount;

        #endregion


        #region Public Properties

        /// <summary>
        /// The count of attendance 
        /// </summary>
        public ChartValues<int> AttendanceCount
        {
            get => attendanceCount;
            set
            {
                attendanceCount = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The count of absence
        /// </summary>
        public ChartValues<int> AbsenceCount
        {
            get => absenceCount;
            set
            {
                absenceCount = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The count of late
        /// </summary>
        public ChartValues<int> LateCount
        {
            get => lateCount;
            set
            {
                lateCount = value;
                RaisePropertyChanged();
            }
        } 

        /// <summary>
        /// The pie chart's label action
        /// </summary>
        public Func<ChartPoint, string> PointLabel
        {
            get => pointLabel;
            set
            {
                pointLabel = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The list of attendance
        /// </summary>
        public string AttendanceList
        {
            get => attendanceList;
            set
            {
                attendanceList = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The list of absence
        /// </summary>
        public string AbsenceList
        {
            get => absenceList;
            set
            {
                absenceList = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The list of late
        /// </summary>
        public string LateList
        {
            get => lateList;
            set
            {
                lateList = value;
                RaisePropertyChanged();
            }
        }


        #endregion


        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public HomeViewModel()
        {
            //Initialize data
            InitData();

            //Initialize the pie chart labels
            PointLabel = chartPoint => string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get the data from database and init it
        /// </summary>
        public async void InitData()
        {
            try
            {
                List<WorkerAttendanceInfoModel> list = workerService.GetWorkerAttendanceList();
                DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                string attendanceTime = "9:30";
                int attendance = 0;
                int absence = 0;
                int late = 0;

                foreach (var info in list)
                {
                    //absence
                    if(DateTime.Compare(info.AttendanceTime, today) < 0)
                    {
                        AbsenceList += info.Name + spearator + info.WorkNumber + "\n";
                        absence++;
                    }
                    else
                    {
                        string currentTime = info.AttendanceTime.Hour + ":" + info.AttendanceTime.Minute;
                        if (DateTime.Compare(Convert.ToDateTime(attendanceTime), Convert.ToDateTime(currentTime)) >= 0) // on time
                        {
                            AttendanceList += info.Name + spearator + info.WorkNumber + "\n";
                            attendance++;
                        }
                        else //late
                        { 
                            LateList += info.Name + spearator + info.WorkNumber + "\n";
                            late++;
                        }
                    }
                }

                AttendanceCount = new ChartValues<int> { attendance };
                AbsenceCount = new ChartValues<int> { absence };
                LateCount = new ChartValues<int> { late };

            }
            catch(Exception e)
            {
                await new MessageDialog("错误", "数据库发生错误：" + e.Message, true).ShowDialog();
            }

        }

        #endregion
    }
}
