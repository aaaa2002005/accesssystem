﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of BaseDialogWindow to implement it's business logic
    /// </summary>
    public sealed class BaseDialogViewModel : ViewModelBase
    {
        #region Private Members

        /// <summary>
        /// The border brush of dialog window
        /// </summary>
        private SolidColorBrush borderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));

        /// <summary>
        /// The border thickness of dialog window
        /// </summary>
        private int borderThickness = 1;

        /// <summary>
        /// The title of dialog window
        /// </summary>
        private string title = null;

        /// <summary>
        /// The title's background color of dialog window
        /// </summary>
        private SolidColorBrush titleBackground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));

        /// <summary>
        /// The title's height of dialog window
        /// </summary>
        private int titleHeight = 40;

        /// <summary>
        /// The width of dialog window
        /// </summary>
        private int windowHeight = 300;

        /// <summary>
        /// The height of dialog window
        /// </summary>
        private int windowWidth = 400;

        /// <summary>
        /// The content of dialog
        /// </summary>
        private Control dialogContent = null;

        /// <summary>
        /// The BaseDialogWindow object
        /// </summary>
        private readonly BaseDialogWindow dialogWindow;

        #endregion

        #region Public Properties

        /// <summary>
        /// The border brush of dialog window
        /// </summary>
        public SolidColorBrush BorderBrush 
        { 
            get => borderBrush; 
            set
            {
                borderBrush = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The border thickness of dialog window
        /// </summary>
        public int BorderThickness
        {
            get => borderThickness;
            set
            {
                borderThickness = value; 
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The title of dialog window
        /// </summary>
        public string Title
        {
            get => title;
            set
            {
                title = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The title's background color of dialog window
        /// </summary>
        public SolidColorBrush TitleBackground
        {
            get => titleBackground;
            set
            {
                titleBackground = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The title's height of dialog window
        /// </summary>
        public int TitleHeight
        {
            get => titleHeight;
            set
            {
                titleHeight = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The width of dialog
        /// </summary>
        public int WindowHeight
        {
            get => windowHeight;
            set
            {
                windowHeight = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The height of dialog
        /// </summary>
        public int WindowWidth
        {
            get => windowWidth;
            set
            {
                windowWidth = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The content of dialog
        /// </summary>
        public Control DialogContent
        {
            get => dialogContent;
            set
            {
                dialogContent = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// The command to close this dialog
        /// </summary>
        public RelayCommand CloseCommand { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dialogWindow"></param>
        public BaseDialogViewModel(BaseDialogWindow dialogWindow)
        {
            //Get the dialog window reference
            this.dialogWindow = dialogWindow;

            //Create close command
            CloseCommand = new RelayCommand(() => this.dialogWindow.Close());
        }

        #endregion
    }
}
