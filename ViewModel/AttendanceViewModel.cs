﻿using AccessControlSystem.ImageProcessing;
using AccessControlSystem.ImageProcessing.Extensions;
using AccessControlSystem.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of AttendancePage to implement it's business logic
    /// </summary>
    public class AttendanceViewModel : ViewModelBase
    {
        #region Private Members

        /// <summary>
        /// The result of face recognition
        /// </summary>
        private bool recognizedResult = false;

        /// <summary>
        /// A photo of database
        /// </summary>
        private BitmapImage databasePhoto;

        /// <summary>
        /// Worker info
        /// </summary>
        private WorkerInfoModel workerInfo;

        /// <summary>
        /// The face photo from CameraBox captured
        /// </summary>
        private BitmapImage facePhoto;

        /// <summary>
        /// Worker service
        /// </summary>
        private readonly WorkerService workerService = new WorkerService();

        #endregion

        #region Public Properties

        /// <summary>
        /// The result of face recognition
        /// </summary>
        public bool RecognizedResult
        {
            get => recognizedResult;
            set
            {
                recognizedResult = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// A photo of database
        /// </summary>
        public BitmapImage DatabasePhoto
        {
            get => databasePhoto;
            set
            {
                databasePhoto = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Worker info
        /// </summary>
        public WorkerInfoModel WorkerInfo
        {
            get => this.workerInfo;
            set
            {
                workerInfo = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// The command to Recognize the newest photo with sql's one
        /// </summary>
        public RelayCommand RecognizeCommand { get; set; }

        /// <summary>
        /// The command of attendance
        /// </summary>
        public RelayCommand AttendCommand { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AttendanceViewModel()
        {
            //Receive the worker info from MainViewModel
            Messenger.Default.Register<WorkerInfoModel>(this, "GetWorkerInfo", GetWorkerInfo);

            //Receive the face photo from CameraBoxViewModel
            Messenger.Default.Register<BitmapImage>(this, "GetFacePhoto", facePhoto => this.facePhoto = facePhoto);

            //Create commands
            RecognizeCommand = new RelayCommand(Recognize);
            AttendCommand = new RelayCommand(Attend);
            
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// Get worker info
        /// </summary>
        /// <param name="workerInfo"></param>
        private void GetWorkerInfo(WorkerInfoModel workerInfo)
        {
            //Update workerInfo and database photo 
            WorkerInfo = workerInfo;
            DatabasePhoto = ImageConverter.ToBitmapImage(workerService.GetWorkerImage(workerInfo.WorkNumber));
        }

        /// <summary>
        /// Recognize the face photo of scene with database's
        /// </summary>
        private async void Recognize()
        {
            if (facePhoto == null)
            {
                await new MessageDialog("警告", "未拍摄人脸图像，无法对比！").ShowDialog();
            }
            else if (!FaceRecognizer.IsCanFaceEncoding(ImageConverter.ToImage(facePhoto)))
            {
                await new MessageDialog("警告", "当前拍摄的照片不符合要求，请重新拍摄！").ShowDialog();
            }
            else
            {
                //Recognize
                RecognizedResult = FaceRecognizer.Recognize(ImageConverter.ToImage(DatabasePhoto), ImageConverter.ToImage(facePhoto));

            }                                    
        }

        /// <summary>
        /// Update the current time to database
        /// </summary>
        private async void Attend()
        {
            if(!RecognizedResult)
            {
                await new MessageDialog("警告", "当前未进行识别或识别未通过！").ShowDialog();
            }
            else
            {
                try
                {
                    workerService.UpdateAttendanceTime(workerInfo.WorkNumber);
                    await new MessageDialog("提示", "考勤成功！").ShowDialog();

                }
                catch(Exception e)
                {
                    await new MessageDialog("错误", "数据库发生错误：" + e.Message, true).ShowDialog();
                }
            }
        }

        #endregion
    }
}
