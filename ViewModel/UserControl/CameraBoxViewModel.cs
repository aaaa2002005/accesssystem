﻿using AccessControlSystem.ImageProcessing;
using AccessControlSystem.ImageProcessing.Extensions;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of CameraBox to implement it's business logic
    /// </summary>
    class CameraBoxViewModel : ViewModelBase
    {
        #region Private Members

        /// <summary>
        /// Camera index
        /// </summary>
        private string cameraIndex = null;

        /// <summary>
        /// The image of show the video captured
        /// </summary>
        private BitmapImage videoSource;

        /// <summary>
        /// The face photo of detected by camera
        /// </summary>
        private BitmapImage facePhoto;

        /// <summary>
        /// Camera task
        /// </summary>
        private Task cameraTask;

        /// <summary>
        /// A camera
        /// </summary>
        private Camera camera;

        #endregion

        #region Public Propeeties

        /// <summary>
        /// Camera index
        /// </summary>
        public string CameraIndex
        {
            get => cameraIndex;
            set
            {
                cameraIndex = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The image to show the video captured
        /// </summary>
        public BitmapImage VideoSource
        {
            get => videoSource;
            set
            {
                videoSource = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The face photo of detected by camera
        /// </summary>
        public BitmapImage FacePhoto
        {
            get => facePhoto;
            set
            {
                facePhoto = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// The command to open camera according the camera index
        /// </summary>
        public RelayCommand OpenCameraCommand { get; set; }

        /// <summary>
        /// The command to take a photo from opened camera
        /// </summary>
        public RelayCommand TakePhotoCommand { get; set; }

        /// <summary>
        /// The command to clear captured photo
        /// </summary>
        public RelayCommand ClearPhotoCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CameraBoxViewModel()
        {
            //Register ExitCameraTask function, it sent by MainViewModel
            Messenger.Default.Register<object>(this, "ExitCameraTask", ExitCameraTask);

            //Create open camera command
            OpenCameraCommand = new RelayCommand(OpenCamera);

            //Create clear photo command
            ClearPhotoCommand = new RelayCommand(() =>
            {
                FacePhoto = null;

                //Send the message to clear face photo
                Messenger.Default.Send<BitmapImage>(null, "GetFacePhoto");
            });

            //Create take photo command
            TakePhotoCommand = new RelayCommand(() =>
            {
                //If camera is no opened, don't update the photo
                if(camera != null && camera.IsOpened)
                {
                    FacePhoto = facePhoto;

                    //Send face photo to outter
                    Messenger.Default.Send<BitmapImage>(FacePhoto, "GetFacePhoto");
                    
                }
            });

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// According camera index to open the camera
        /// </summary>
        private async void OpenCamera()
        {
            //await the camera task
            ExitCameraTask(null);

            //Judge whether camera index is legal
            if (cameraIndex != null && Camera.GetAllCameraIndexes().Contains(int.Parse(cameraIndex)))
            {
                //Create new camera            
                camera = new Camera(int.Parse(cameraIndex));

                //Assign camera task to detect face
                cameraTask = Task.Run(() =>
                {
                    //When detected face, the facePhoto is updated
                    while (camera.IsOpened)
                    {
                        var frame = camera.Read();
                        if (frame == null)
                        {
                            continue;
                        }

                        var sceneImage = ImageConverter.ToBitmapImage(frame);
                        if(FaceRecognizer.DetectFace(frame, out BitmapImage outputImage))
                        {
                            VideoSource = outputImage;
                            facePhoto = sceneImage;
                        }
                        else
                        {
                            VideoSource = sceneImage;
                        }
                    }
                });
            }
            else
            {
                await new MessageDialog("警告", "当前输入的索引号不存在！").ShowDialog();
            }

        }

        /// <summary>
        /// Exit the camera task
        /// </summary>
        /// <param name="sender">null</param>
        private void ExitCameraTask(object sender)
        {
            if (camera != null && camera.IsOpened)
            {
                camera.Close();
                cameraTask.Wait();
                camera.Release();
            }
        }

        #endregion
    }
}
