﻿using AccessControlSystem.ImageProcessing.Extensions;
using AccessControlSystem.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of ManageDialog to implement it's business logic
    /// </summary>
    public class ManageDialogViewModel : ViewModelBase
    {

        #region Private Members

        /// <summary>
        /// Worker's info
        /// </summary>
        private WorkerInfoModel workerInfo;

        /// <summary>
        /// WorkNumber
        /// </summary>
        private string workNumberText;

        /// <summary>
        /// The database's photo
        /// </summary>
        private BitmapImage databasePhoto;

        /// <summary>
        /// Worker's service
        /// </summary>
        private WorkerService workerService = new WorkerService();

        #endregion


        #region Public properties

        /// <summary>
        /// Worker's info
        /// </summary>
        public WorkerInfoModel WorkerInfo
        {
            get => workerInfo;
            set
            {
                workerInfo = value;
                RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// WorkNumber
        /// </summary>
        public string WorkNumberText
        {
            get => workNumberText;
            set
            {
                workNumberText = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The database's photo
        /// </summary>
        public BitmapImage DatabasePhoto
        {
            get => databasePhoto;
            set
            {
                databasePhoto = value;
                RaisePropertyChanged();
            }
        }

        #endregion


        #region Commands
        
        /// <summary>
        /// The command to modify worker's info
        /// </summary>
        public RelayCommand ModifyCommand { get; set; }

        /// <summary>
        /// The command to delete worker's info
        /// </summary>
        public RelayCommand DeleteCommand { get; set; }

        /// <summary>
        /// The command to query worker's info
        /// </summary>
        public RelayCommand QueryCommand { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ManageDialogViewModel()
        {
            //Create commands
            QueryCommand = new RelayCommand(Query);
            ModifyCommand = new RelayCommand(Modify);
            DeleteCommand = new RelayCommand(Delete);
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// According the workNumber query worker's info
        /// </summary>
        private async void Query()
        {
            try
            {
                if (!workerService.IsWorkNumberExist(workNumberText))
                {
                    await new MessageDialog("警告", "当前工号不存在请重新输入！").ShowDialog();
                }
                else
                {
                    WorkerInfo = workerService.GetWorkerInfo(workNumberText);
                    DatabasePhoto = ImageConverter.ToBitmapImage(workerService.GetWorkerImage(workNumberText));
                }
            }
            catch(Exception e)
            {
                await new MessageDialog("错误", "数据库发生错误：" + e.Message, true).ShowDialog();
            }
        }

        /// <summary>
        /// Modify corresponding worker's info
        /// </summary>
        private async void Modify()
        {
            if (WorkerInfo == null)
            {
                await new MessageDialog("警告", "未确定工号无法操作").ShowDialog();
            }
            else
            {
                try
                {
                    workerService.UpdateWorkerInfo(WorkerInfo);
                }
                catch(Exception e)
                {
                    await new MessageDialog("错误", "数据库发生错误：" + e.Message, true).ShowDialog();
                    return;
                }
                await new MessageDialog("提示", "修改成功！").ShowDialog();
            }
        }

        /// <summary>
        /// Delete corresponding worker's info
        /// </summary>
        private async void Delete()
        {
            if(WorkerInfo == null)
            {
                await new MessageDialog("警告", "未确定工号无法操作").ShowDialog();
            }
            else
            {
                try
                {
                    workerService.DeleteWorkerInfo(WorkerInfo.WorkNumber);
                }
                catch (Exception e)
                {
                    await new MessageDialog("错误", "数据库发生错误：" + e.Message, true).ShowDialog();
                    return;
                }

                await new MessageDialog("提示", "删除成功！").ShowDialog();
                WorkerInfo = null;
                DatabasePhoto = null;
                WorkNumberText = null;
            }
        }

        #endregion

    }
}
