﻿using AccessControlSystem.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of AttendanceMessageDialog
    /// </summary>
    public class AttendanceMessageDialogViewModel : ViewModelBase
    {
        #region Private Members

        /// <summary>
        /// Current dialog
        /// </summary>
        private readonly AttendanceMessageDialog dialog;

        #endregion

        #region Public Properties

        /// <summary>
        /// Worker info
        /// </summary>
        public WorkerInfoModel WorkerInfo { get; set; } = new WorkerInfoModel();
        
        #endregion

        #region Commands

        /// <summary>
        /// The command of confirm
        /// </summary>
        public RelayCommand ConfirmCommand { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Consturctor
        /// </summary>
        public AttendanceMessageDialogViewModel(AttendanceMessageDialog dialog)
        {
            //Get the dialog reference
            this.dialog = dialog;

            //Create the ConfirmCommand
            ConfirmCommand = new RelayCommand(Confirm);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Confirm the user name and user number whether exist
        /// </summary>
        private async void Confirm()
        {
            //Validate the input message
            WorkerService service = new WorkerService();
            try
            {
                string message = null;
                if(WorkerInfo.Name == null || WorkerInfo.WorkNumber == null)
                {
                    message = "输入不能为空！";
                }
                else if(!service.IsWorkNumberExist(WorkerInfo.WorkNumber))
                {
                    message = "当前工号不存在！";
                }
                else
                {
                    var workerInfoDB = service.GetWorkerInfo(WorkerInfo.WorkNumber);
                    if (workerInfoDB.Name != WorkerInfo.Name) //Name can't match
                    {
                        message = "当前姓名与数据库中不匹配！";
                    }
                    else
                    {
                        //Update worker info
                        WorkerInfo = workerInfoDB;
                    }
                }

                if(message != null)
                {
                    await new MessageDialog("错误", message).ShowDialog();
                    return;
                }
            }
            catch(Exception e)
            {
                await new MessageDialog("错误", "查询工号信息发生错误，具体信息：" + e.Message).ShowDialog();
                return;
            }

            //Send the worker info to MainViewModel
            Messenger.Default.Send<WorkerInfoModel>(WorkerInfo, "GetWorkerInfo");

            //Finish it
            dialog.FinishDialog();
        }
        #endregion
    }
}
