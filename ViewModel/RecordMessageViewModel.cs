﻿using AccessControlSystem.ImageProcessing;
using AccessControlSystem.ImageProcessing.Extensions;
using AccessControlSystem.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of RecordMessagePage to implement it's business logic
    /// </summary>
    class RecordMessageViewModel : ViewModelBase
    {
        #region Private Members

        /// <summary>
        /// Face photo
        /// </summary>
        private BitmapImage facePhoto;

        #endregion

        #region Public Properties

        /// <summary>
        /// Worker info 
        /// </summary>
        public WorkerInfoModel WorkerInfo { get; private set; } = new WorkerInfoModel();
 
        #endregion

        #region Commands

        /// <summary>
        /// The command to record message
        /// </summary>
        public RelayCommand RecordMessageCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public RecordMessageViewModel()
        {
            //Receive face photo from CameraBoxViewModel
            Messenger.Default.Register<BitmapImage>(this, "GetFacePhoto", facePhoto => this.facePhoto = facePhoto);

            RecordMessageCommand = new RelayCommand(Record);
        }

        #endregion


        #region Private Methods

        private async void Record()
        {
            //Judge input message whether legal

            //Add the worker info to database
            WorkerService service = new WorkerService();
            try
            {
                string message = null;
                if (service.IsWorkNumberExist(WorkerInfo.WorkNumber))
                {
                    message = "当前的工号已被注册，无法录入信息！";
                }
                else if (facePhoto == null)
                {
                    message = "未拍摄对应的人脸照片，无法录入信息！";
                }
                else if (!FaceRecognizer.IsCanFaceEncoding(ImageConverter.ToImage(facePhoto)))
                {
                    message = "当前拍摄的照片不符合规范，请重新拍摄！";
                }
                else
                {
                    service.AddWorkerInfo(WorkerInfo);
                    service.WriteWorkerImage(WorkerInfo.WorkNumber, ImageConverter.ToMat(facePhoto));
                }

                if(message != null)
                {
                    await new MessageDialog("警告", message).ShowDialog();
                }
                else 
                {
                    await new MessageDialog("提示", "录入成功").ShowDialog();
                }
            }
            catch(Exception e)
            {
                await new MessageDialog("错误", "数据库发生错误：" + e.Message, true).ShowDialog();
            }

        }

        #endregion
    }
}
