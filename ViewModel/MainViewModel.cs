﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AccessControlSystem
{
    /// <summary>
    /// The view model of MainWindow to implement it's business logic
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Private Members

        /// <summary>
        /// The MainWindow's top menu model collection
        /// </summary>
        private ObservableCollection<MenuModel> menuModels;

        /// <summary>
        /// The page of corresponding menu
        /// Note:
        /// The default page is using home page
        /// </summary>
        private Page menuPage = new HomePage();

        /// <summary>
        /// The worker info to send to AttendancePageViewModel
        /// </summary>
        private WorkerInfoModel workerInfo = null;

        /// <summary>
        /// A page dictionary to create corresponding page
        /// </summary>
        private readonly Dictionary<string, Type> pageDictionary;

        #endregion

        #region Public Properties

        /// <summary>
        /// The MainWindow's top menu model collection
        /// </summary>
        public ObservableCollection<MenuModel> MenuModels
        {
            get => menuModels;
            set
            {
                menuModels = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The page of corresponding menu
        /// </summary>
        public Page MenuPage
        {
            get => menuPage;
            set
            {
                menuPage = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// The command to switch page of MainWindow
        /// </summary>
        public RelayCommand<string> SwitchPageCommand { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MainViewModel()
        {

            //The "\xe***" means the unicode-16 code which correspond the icon from #IconFonts
            menuModels = new ObservableCollection<MenuModel>
            {
                //Home
                new MenuModel()
                {
                    Icon = "\xe7bb",
                    Title = "主页",
                    IsChosen = true
                },

                //Attendance
                new MenuModel()
                {
                    Icon = "\xe6b6",
                    Title = "考勤"
                },

                //Record
                new MenuModel()
                {
                    Icon = "\xe65c",
                    Title = "录入"
                },
                
                //Manage
                new MenuModel()
                {
                    Icon = "\xe62b",
                    Title = "管理"
                },

                //Exit
                new MenuModel()
                {
                    Icon = "\xe60d",
                    Title = "退出"
                }

            };

            //Add the page
            pageDictionary = new Dictionary<string, Type>()
            {
                {"主页", typeof(HomePage) },

                {"考勤", typeof(AttendancePage) },

                {"录入", typeof(RecordMessagePage) },

            };

            //initialize MainWindow's commands
            SwitchPageCommand = new RelayCommand<string>(content => SwitchPage(content));

            //Resigter the GetWorkerInfo function, it sent by AttendanceMessageDialogViewModel
            Messenger.Default.Register<WorkerInfoModel>(this, "GetWorkerInfo", workerInfo => this.workerInfo = workerInfo);

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Switch the page of frame source according the radio button content
        /// </summary>
        /// <param name="source">Radio button content</param>
        private async void SwitchPage(string content)
        {
            if (content == "退出")
            {
                Application.Current.Shutdown();
            }
            else if(content == "管理")
            {
                await new ManageDialog().ShowDialog();
            }
            else
            {
                //Update the menu IsSelected
                var enumerator = MenuModels.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Title != content)
                    {
                        enumerator.Current.IsChosen = false;
                    }
                    else
                    {
                        enumerator.Current.IsChosen = true;
                    }
                }

                if (pageDictionary.TryGetValue(content, out Type pageType))
                {
                    //Using reflection to get the type
                    Page page = (Page)Activator.CreateInstance(pageType);

                    //When the page is different, change it
                    if (page.GetType().Name != MenuPage.GetType().Name)
                    {
                        //It register in the CameraBoxViewModel
                        Messenger.Default.Send<object>(null, "ExitCameraTask");

                        //The AttendancePage must 
                        if (content == "考勤")
                        {
                            if (await new AttendanceMessageDialog().ShowDialog())
                            {
                                //Send the worker info to AttendanceViewModel
                                Messenger.Default.Send<WorkerInfoModel>(this.workerInfo, "GetWorkerInfo");
                            }
                            else
                            {
                                //If Dialog result is false, switch back to HomePage
                                SwitchPage("主页");
                                return;
                            }
                        }

                        MenuPage = page;
                    }
                }
            }


        }

        #endregion
    }
}
