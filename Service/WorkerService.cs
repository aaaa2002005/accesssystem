﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AccessControlSystem.Service
{
    /// <summary>
    /// DAL for worker
    /// </summary>
    public class WorkerService
    {
        #region Private Members

        /// <summary>
        /// The image folder path
        /// </summary>
        private readonly string imageFolderPath = @"E:\Access Control System Images\";

        /// <summary>
        /// The image format suffix
        /// </summary>
        private readonly string imageFormatSuffix = ".jpg";

        #endregion

        #region Public Methods

        /// <summary>
        /// Get worker's image
        /// </summary>
        /// <param name="workNumber"></param>
        /// <returns></returns>
        public Mat GetWorkerImage(string workNumber)
        {
            return Cv2.ImRead(imageFolderPath + workNumber + imageFormatSuffix);
        }

        /// <summary>
        /// Write the image of worker
        /// </summary>
        /// <param name="workNumber"></param>
        /// <param name="image"></param>
        public void WriteWorkerImage(string workNumber, Mat image)
        {
            Cv2.ImWrite(imageFolderPath + workNumber + imageFormatSuffix, image);
        }

        /// <summary>
        /// Query the work number whether is registered
        /// </summary>
        /// <param name="workNumber"></param>
        /// <returns></returns>
        public bool IsWorkNumberExist(string workNumber)
        {
            //Write the sql
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("select count(*) from WorkerInfoTable where WorkNumber = '{0}' ");
            string sql = string.Format(sqlBuilder.ToString(), workNumber);

            //Query from database
            try
            {
                int result = (int)SqlHelper.GetSingleResult(sql);
                return result == 1 ? true : false;
            }
            catch (Exception e)
            {
                throw e;
            }
            
        }

        /// <summary>
        /// According the work number to get the WorkerInfo
        /// </summary>
        /// <param name="workNumber"></param>
        /// <returns></returns>
        public WorkerInfoModel GetWorkerInfo(string workNumber)
        {
            //Write the sql
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("select Name, Gender, BirthDay, WorkNumber, PhoneNumber from WorkerInfoTable where WorkNumber = '{0}' ");
            string sql = string.Format(sqlBuilder.ToString(), workNumber);
            
            //Query
            try
            {
                SqlDataReader reader = SqlHelper.GetReader(sql);
                WorkerInfoModel workerInfo = new WorkerInfoModel();
                if(reader.Read())
                {
                    workerInfo.Name = reader["Name"].ToString();
                    workerInfo.Gender = reader["Gender"].ToString();
                    workerInfo.BirthDay = reader["BirthDay"].ToString();
                    workerInfo.WorkNumber = reader["WorkNumber"].ToString();
                    workerInfo.PhoneNumber = reader["PhoneNumber"].ToString();

                }
                return workerInfo;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get the worker attendance list
        /// </summary>
        /// <returns></returns>
        public List<WorkerAttendanceInfoModel> GetWorkerAttendanceList()
        {
            var list = new List<WorkerAttendanceInfoModel>();
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("select WorkerInfoTable.WorkNumber, Name, AttendanceTime from WorkerInfoTable ");
            sqlBuilder.Append("inner join WorkerAttendanceTable on WorkerInfoTable.WorkNumber = WorkerAttendanceTable.WorkNumber");
            string sql = string.Format(sqlBuilder.ToString());
            SqlDataReader reader = null;

            try
            {
                reader = SqlHelper.GetReader(sql);
                while(reader.Read())
                {
                    list.Add(new WorkerAttendanceInfoModel()
                    {
                        Name = (string)reader["Name"],
                        WorkNumber = reader["WorkNumber"].ToString(),
                        AttendanceTime = (DateTime)reader["AttendanceTime"]
                    });
                }
                
            }
            catch(Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Close();
            }

            return list;
        }


        /// <summary>
        /// Add worker info to database
        /// </summary>
        /// <param name="workerInfo">Worker info</param>
        public void AddWorkerInfo(WorkerInfoModel workerInfo)
        {
            //Write the sql
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("Insert into WorkerInfoTable(Name, Gender, BirthDay, WorkNumber, PhoneNumber) ");
            sqlBuilder.Append("values('{0}', '{1}', '{2}', '{3}', '{4}'); ");
            sqlBuilder.Append("Insert into WorkerAttendanceTable(WorkNumber) ");
            sqlBuilder.Append("values('{3}')");
            string sql = string.Format(sqlBuilder.ToString(), workerInfo.Name, workerInfo.Gender, workerInfo.BirthDay, workerInfo.WorkNumber, workerInfo.PhoneNumber);

            //Update to database
            try
            {
                SqlHelper.Update(sql);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Delete worker's info from database
        /// </summary>
        /// <param name="workerInfo"></param>
        public void DeleteWorkerInfo(string workNumber)
        {
            //Write the sql
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("Delete from WorkerAttendanceTable where WorkNumber = '{0}'; ");
            sqlBuilder.Append("Delete from WorkerInfoTable where WorkNumber = '{0}' ");
            string sql = string.Format(sqlBuilder.ToString(), workNumber);

            try
            {
                SqlHelper.Update(sql);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Update wokrer's info from database
        /// </summary>
        /// <param name="workerInfo"></param>
        public void UpdateWorkerInfo(WorkerInfoModel workerInfo)
        {
            //Write the sql
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("Update WorkerInfoTable set Name = '{0}', Gender = '{1}', BirthDay = '{2}', PhoneNumber = '{3}' ");
            sqlBuilder.Append("where WorkNumber = '{4}'");
            string sql = string.Format(sqlBuilder.ToString(), workerInfo.Name, workerInfo.Gender, workerInfo.BirthDay, workerInfo.PhoneNumber, workerInfo.WorkNumber);

            try
            {
                SqlHelper.Update(sql);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Update corresponding work number's attendance time
        /// </summary>
        /// <param name="workNumber"></param>
        public void UpdateAttendanceTime(string workNumber)
        {
            //Write the sql
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append("Update WorkerAttendanceTable set AttendanceTime = '{0}' where WorkNumber = '{1}';");
            string sql = string.Format(sqlBuilder.ToString(), DateTime.Now.ToString(), workNumber);

            try
            {
                SqlHelper.Update(sql);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        

        #endregion
    }
}
