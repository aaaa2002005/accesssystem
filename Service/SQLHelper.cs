﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace AccessControlSystem.Service
{
    /// <summary>
    /// Database access helper class
    /// </summary>
    public static class SqlHelper
    {
        #region Private Members

        /// <summary>
        /// The string to connect database
        /// </summary>
        private readonly static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

        #endregion


        #region Public Helper Methods of Database Access 

        /// <summary>
        /// Using sql sentence to operate database(add, modify, delete, query)
        /// </summary>
        /// <param name="sql">Sql sentence</param>
        /// <returns>The affected line count</returns>
        public static int Update(string sql)
        {
            var sqlConnection = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand(sql, sqlConnection);

            try
            {
                sqlConnection.Open();
                return sqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        /// <summary>
        /// Using sql sentence to get the single result of query
        /// </summary>
        /// <param name="sql">Sql sentence</param>
        /// <returns>
        /// Queried Result
        /// </returns>
        public static object GetSingleResult(string sql)
        {
            var sqlConnection = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand(sql, sqlConnection);

            try
            {
                sqlConnection.Open();
                return sqlCommand.ExecuteScalar();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }


        /// <summary>
        ///  Using sql sentence to operate database
        /// </summary>
        /// <param name="sql">Sql sentence</param>
        /// <returns>
        /// The SqlDataReader
        /// Note:
        /// It must be closed after using
        /// </returns>
        public static SqlDataReader GetReader(string sql)
        {
            var sqlConnection = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand(sql, sqlConnection);

            try
            {
                sqlConnection.Open();
                return sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception e)
            {
                if(sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
                throw e;
            }
        }

        /// <summary>
        ///  Using sql sentence to operate database
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string sql)
        {
            var sqlConnection = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand(sql, sqlConnection);
            var sqlDataAdapter = new SqlDataAdapter();
            var dataSet = new DataSet();

            try
            {
                sqlConnection.Open();
                sqlDataAdapter.Fill(dataSet);
                return dataSet;
            }
            catch(Exception e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }



        #endregion
    }
}
