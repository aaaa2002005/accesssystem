﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessControlSystem
{
    /// <summary>
    /// The worker's attendance info model
    /// </summary>
    public class WorkerAttendanceInfoModel
    {
        /// <summary>
        /// Worker name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// WorkNumber
        /// </summary>
        public string WorkNumber { get; set; }

        /// <summary>
        /// The date time of attendance
        /// </summary>
        public DateTime AttendanceTime { get; set; }
    }
}
