﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessControlSystem
{
    /// <summary>
    /// The worker's info model
    /// </summary>
    public class WorkerInfoModel
    {
        /// <summary>
        /// Worker name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Worker number
        /// </summary>
        public string WorkNumber { get; set; }

        /// <summary>
        /// Worker gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Worker mobile phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Worker birthday
        /// </summary>
        public string BirthDay { get; set; }
    }
}
