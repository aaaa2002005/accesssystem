﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControlSystem
{
    /// <summary>
    /// The MainWindow's top menu model
    /// </summary>
    public class MenuModel : ObservableObject
    {
        #region Private Members

        /// <summary>
        /// The icon of menu
        /// </summary>
        private string icon;

        /// <summary>
        /// The Title of menu
        /// </summary>
        private string title;

        /// <summary>
        /// Whether the menu is chosen
        /// </summary>
        private bool isChosen = false;

        #endregion


        #region Public Properties

        /// <summary>
        /// The icon of menu
        /// </summary>
        public string Icon
        {
            get => icon;
            set
            {
                icon = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The Title of menu
        /// </summary>
        public string Title
        {
            get => title;
            set
            {
                title = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Whether the menu is chosen
        /// </summary>
        public bool IsChosen
        {
            get => isChosen;
            set
            {
                isChosen = value;
                RaisePropertyChanged();
            }
        }

        #endregion

    }
}
