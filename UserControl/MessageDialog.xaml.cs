﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccessControlSystem
{
    /// <summary>
    /// Interaction logic for MessageDialog.xaml
    /// </summary>
    public partial class MessageDialog : BaseDialog
    {
        #region Dependency Properties

        /// <summary>
        /// MessageDialog's message dependency property
        /// </summary>
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(MessageDialog));

        /// <summary>
        /// MessageDialog's confirm button visibility property
        /// </summary> 
        public static readonly DependencyProperty ConfirmButtonVisibilityProperty =
            DependencyProperty.Register("ConfirmButtonVisibility", typeof(bool), typeof(MessageDialog));

        #endregion

        #region Public Properties

        /// <summary>
        /// MessageDialog's message
        /// </summary>
        public string Message
        {
            get => (string)GetValue(MessageProperty);
            set => SetValue(MessageProperty, value);
        }

        /// <summary>
        ///  MessageDialog's confirm button visibility
        /// </summary>
        public bool ConfirmButtonVisibility
        {
            get => (bool)GetValue(ConfirmButtonVisibilityProperty);
            set => SetValue(ConfirmButtonVisibilityProperty, value);
        }

        /// <summary>
        /// The command of finish this dialog
        /// </summary>
        public RelayCommand FinishCommand { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="title">Dialog title</param>
        /// <param name="message">Dialog message</param>
        /// <param name="isShowConfirmButton">Confirm button visibility</param>
        public MessageDialog(string title, string message, bool isShowConfirmButton = false)
        {
            InitializeComponent();
            this.Title = title;
            this.Message = message;
            this.ConfirmButtonVisibility = isShowConfirmButton;
            this.WindowWidth = 250;
            this.WindowHeight = 150;
            this.FinishCommand = new RelayCommand(this.FinishDialog);
            this.DataContext = this;   
        }

        #endregion


    }
}
