﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccessControlSystem
{
    /// <summary>
    /// Interaction logic for HintTextBox.xaml
    /// </summary>
    public partial class HintTextBox : UserControl
    {
        #region Dependency Properties

        /// <summary>
        /// Hint text depandency property
        /// </summary>
        public static readonly DependencyProperty HintTextProperty =
            DependencyProperty.Register("HintText", typeof(string), typeof(HintTextBox));

        /// <summary>
        /// Hint font size dependency property
        /// </summary>
        public static readonly DependencyProperty HintFontSizeProperty =
            DependencyProperty.Register("HintFontSize", typeof(int), typeof(HintTextBox));

        /// <summary>
        /// Text dependency property
        /// </summary>
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(HintTextBox));

        /// <summary>
        /// Width dependency property
        /// </summary>
        public static readonly new DependencyProperty WidthProperty =
            DependencyProperty.Register("Width", typeof(int), typeof(HintTextBox));

        /// <summary>
        /// BorderThick dependency property
        /// </summary>
        public static readonly new DependencyProperty BorderThicknessProperty = 
            DependencyProperty.Register("BorderThickness", typeof(Thickness), typeof(HintTextBox));

        /// <summary>
        /// BorderBrush dependency property
        /// </summary>
        public static readonly new DependencyProperty BorderBrushProperty =
            DependencyProperty.Register("BorderBrush", typeof(SolidColorBrush), typeof(HintTextBox));



        #endregion

        #region Public Properties

        /// <summary>
        /// Hint text
        /// </summary>
        public string HintText
        {
            get => (string)GetValue(HintTextProperty);
            set => SetValue(HintTextProperty, value);
        }

        /// <summary>
        /// Hint font size
        /// </summary>
        public int HintFontSize
        {
            get => (int)GetValue(HintFontSizeProperty);
            set => SetValue(HintFontSizeProperty, value);
        }

        /// <summary>
        /// Text
        /// </summary>
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        /// <summary>
        /// Text box width
        /// </summary>
        public new int Width
        {
            get => (int)GetValue(WidthProperty);
            set => SetValue(WidthProperty, value);
        }

        /// <summary>
        /// Box border thickness
        /// </summary>
        public new Thickness BorderThickness
        {
            get => (Thickness)GetValue(BorderThicknessProperty);
            set => SetValue(BorderThicknessProperty, value);
        }

        /// <summary>
        /// Box border brush
        /// </summary>
        public new SolidColorBrush BorderBrush
        {
            get => (SolidColorBrush)GetValue(BorderThicknessProperty);
            set => SetValue(BorderThicknessProperty, value);
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public HintTextBox()
        {
            InitializeComponent();
        }

        #endregion
    }
}
