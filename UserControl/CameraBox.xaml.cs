﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccessControlSystem
{
    /// <summary>
    /// Interaction logic for CameraBox
    /// </summary>
    public partial class CameraBox : UserControl
    {

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CameraBox()
        {
            InitializeComponent();
            this.DataContext = new CameraBoxViewModel();
        }
    }
}
