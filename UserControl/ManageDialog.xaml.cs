﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccessControlSystem
{
    /// <summary>
    /// ManageDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ManageDialog : BaseDialog
    {
        /// <summary>
        /// Interaction logic for ManageDialog.xaml
        /// </summary>
        public ManageDialog()
        {
            InitializeComponent();
            InitProperties();
            this.DataContext = new ManageDialogViewModel();
        }

        /// <summary>
        /// Init dialog properties
        /// </summary>
        private void InitProperties()
        {
            this.Title = "管理相应人员信息";
            this.TitleHeight = 40;
            this.WindowHeight = 550;
            this.WindowWidth = 300;
            this.TitleBackground = (SolidColorBrush)this.FindResource("DarkGrayBrush");
        }
    }
}
