﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccessControlSystem
{
    /// <summary>
    /// Interaction logic for AttendanceMessageDialog
    /// </summary>
    public partial class AttendanceMessageDialog : BaseDialog
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public AttendanceMessageDialog()
        {
            InitDialogWindowProperties();
            InitializeComponent();
            this.DataContext = new AttendanceMessageDialogViewModel(this);
        }

        /// <summary>
        /// Init the dialog window properties
        /// </summary>
        private void InitDialogWindowProperties()
        {
            this.Title = "请输入考勤人员信息";
            this.WindowHeight = 250;
            this.WindowWidth = 300;
            this.TitleBackground = (SolidColorBrush)this.FindResource("DarkGrayBrush");
        }
    }
}
