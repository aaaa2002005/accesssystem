﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AccessControlSystem
{
    /// <summary>
    /// The base class of dialog
    /// </summary>
    public class BaseDialog : UserControl
    {
        #region Private Members

        /// <summary>
        /// The dialog window
        /// </summary>
        private readonly BaseDialogWindow dialogWindow;

        #endregion


        #region Public Properties

        /// <summary>
        /// The border brush of dialog window
        /// </summary>
        public new SolidColorBrush BorderBrush
        {
            get => dialogWindow.ViewModel.BorderBrush;
            set => dialogWindow.ViewModel.BorderBrush = value;
        }

        /// <summary>
        /// The border thickness of dialog window
        /// </summary>
        public new int BorderThickness
        {
            get => dialogWindow.ViewModel.BorderThickness;
            set => dialogWindow.ViewModel.BorderThickness = value;
        }

        /// <summary>
        /// The title of dialog
        /// </summary>
        public string Title
        {
            get => dialogWindow.ViewModel.Title;
            set => dialogWindow.ViewModel.Title = value;
        }

        /// <summary>
        /// The title's background color of dialog
        /// </summary>
        public SolidColorBrush TitleBackground
        {
            get => dialogWindow.ViewModel.TitleBackground;
            set => dialogWindow.ViewModel.TitleBackground = value;
        }

        /// <summary>
        /// The title's height of dialog
        /// </summary>
        public int TitleHeight
        {
            get => dialogWindow.ViewModel.TitleHeight;
            set => dialogWindow.ViewModel.TitleHeight = value;
        }

        /// <summary>
        /// The height of dialog
        /// </summary>
        public int WindowHeight
        {
            get => dialogWindow.ViewModel.WindowHeight;
            set => dialogWindow.ViewModel.WindowHeight = value;
        }

        /// <summary>
        /// The width of dialog
        /// </summary>
        public int WindowWidth
        {
            get => dialogWindow.ViewModel.WindowWidth;
            set => dialogWindow.ViewModel.WindowWidth = value;
        }

        /// <summary>
        /// The content of dialog
        /// </summary>
        public Control DialogContent
        {
            get => dialogWindow.ViewModel.DialogContent;
            set => dialogWindow.ViewModel.DialogContent = value;
        }


        #endregion


        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public BaseDialog()
        {
            //Create a dialog window
            dialogWindow = new BaseDialogWindow();
            dialogWindow.ViewModel = new BaseDialogViewModel(dialogWindow);

        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Show the dialog
        /// </summary>
        /// <returns>
        /// If true means it is finished state, else closed
        /// </returns>
        public Task<bool> ShowDialog()
        {
            //Create a task to await the dialog closing, get the dialog result
            var tcs = new TaskCompletionSource<bool>();
            bool? result = null;

            try
            {
                //Run on UI thread to show the dialog
                Application.Current.Dispatcher.Invoke(() =>
                {
                    DialogContent = this;
                    result = dialogWindow.ShowDialog();
                });
            }
            finally
            {
                tcs.TrySetResult((bool)result);
            }

            return tcs.Task;
        }

        /// <summary>
        /// Close the dialog
        /// </summary>
        public void CloseDialog()
        {
            dialogWindow.Close();
        }

        /// <summary>
        /// Finish the dialog's business logic and close it
        /// </summary>
        public void FinishDialog()
        {
            dialogWindow.Finish();
        }

        #endregion
    }
}
